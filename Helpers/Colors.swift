//
//  Colors.swift
//  SafeBodaTask
//
//  Created by DXBSS-MACLTP on 10/6/19.
//  Copyright © 2019 DXBSS-MACLTP. All rights reserved.
//

import UIKit

class Colors: NSObject {
    
    static let transparentWhiteColor = UIColor.init(white: 1, alpha: 0.6)
    
    static let textFocusColor = UIColor.init(displayP3Red: 68/255, green: 77/255, blue: 81/255, alpha: 1.0)
    
    static let textUnfocusColor = UIColor.init(displayP3Red: 82/255, green: 96/255, blue: 100/255, alpha: 1.0)
    
    class func changePlaceHolderColor (textField : UITextField , color : UIColor) {
        textField.attributedPlaceholder = NSAttributedString(string: textField.placeholder!, attributes: [NSAttributedString.Key.foregroundColor: color])
    }
    
    
}
