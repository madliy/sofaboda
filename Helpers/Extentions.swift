//
//  Extentions.swift
//  SafeBodaTask
//
//  Created by DXBSS-MACLTP on 12/10/2019.
//  Copyright © 2019 DXBSS-MACLTP. All rights reserved.
//

import UIKit

class Extentions: NSObject {

}
extension Date {
    func format(dateFormat : String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: self)
    }
}
extension String {
    
    var Date : String {
        let array = self.components(separatedBy: "T")
        if(array.count > 0) {
            return array[0]
        }
        return self
    }
    
    var Time : String {
        let array = self.components(separatedBy: "T")
        if(array.count > 1) {
            return array[1]
        }
        return self
    }
    
}
