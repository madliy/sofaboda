//
//  FlightsViewController.swift
//  Sofa
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright (c) 2019 DXBSS-MACLTP. All rights reserved.
//  Modify By:  * Ari Munandar
//              * arimunandar.dev@gmail.com
//              * https://github.com/arimunandar

import UIKit
protocol FlightTableDelegate {
    func selectJourney()
}
protocol IFlightsViewController: class {
	var router: IFlightsRouter? { get set }
    func reloadData(array : [Journey])
}

class FlightsViewController: UITableViewController {
	var interactor: IFlightsInteractor?
	var router: IFlightsRouter?
    
    
    private var journeysArray = [Journey]()
    
    var delegate: FlightTableDelegate?

	override func viewDidLoad() {
        super.viewDidLoad()
        FlightsConfiguration.initValues(controller: self)
    }
    
    
    // MARK: - Table view data source
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
         return journeysArray.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return journeysArray[section].journeyCode
    }
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let journey = journeysArray[section]
        return journey.flightsArray.count
    }

    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FlightTableViewCell", for: indexPath) as? FlightTableViewCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        let journey = journeysArray[indexPath.section]
        let flight = journey.flightsArray[indexPath.row]
        cell.titleLabel.text = "\(flight.departureAirportCode) -- \( flight.arrivalAirportCode)"
        if(flight.departureDate == flight.arrivalDate) {
            cell.dateLabel.text = "\(flight.departureDate)"
        }
        else {
            cell.dateLabel.text = "\(flight.departureDate) - \( flight.arrivalDate)"
        }
        
        cell.timeLabel.text = "\(flight.departureTime) - \( flight.arrivalTime)"
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.selectJourney()
    }
    
    func reloadData(array : [Journey]) {
        journeysArray = array
        self.tableView.reloadData()
    }
}

extension FlightsViewController: IFlightsViewController {
	// do someting...
}

extension FlightsViewController {
	// do someting...
}

extension FlightsViewController {
	// do someting...
}
