//
//  FlightsPresenter.swift
//  Sofa
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright (c) 2019 DXBSS-MACLTP. All rights reserved.
//  Modify By:  * Ari Munandar
//              * arimunandar.dev@gmail.com
//              * https://github.com/arimunandar

import UIKit

protocol IFlightsPresenter: class {
	func setJourneyList(journeyList: [Journey])
}

class FlightsPresenter: IFlightsPresenter {	
	weak var view: IFlightsViewController?
	
	init(view: IFlightsViewController?) {
		self.view = view
	}
    
    func setJourneyList(journeyList: [Journey]) {
        view?.reloadData(array: journeyList)
    }
    
}
