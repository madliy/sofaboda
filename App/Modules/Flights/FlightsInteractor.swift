//
//  FlightsInteractor.swift
//  Sofa
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright (c) 2019 DXBSS-MACLTP. All rights reserved.
//  Modify By:  * Ari Munandar
//              * arimunandar.dev@gmail.com
//              * https://github.com/arimunandar

import UIKit

protocol IFlightsInteractor: class {
	var parameters: [String: Any]? { get set }
    func setJourneyList(journeyList: [Journey])
}

class FlightsInteractor: IFlightsInteractor {
    var presenter: IFlightsPresenter?
    var manager: IFlightsManager?
    var parameters: [String: Any]?

    init(presenter: IFlightsPresenter, manager: IFlightsManager) {
    	self.presenter = presenter
    	self.manager = manager
    }
    
    func setJourneyList(journeyList: [Journey]) {
        presenter?.setJourneyList(journeyList: journeyList)
    }
}
