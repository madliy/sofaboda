//
//  FlightsRouter.swift
//  Sofa
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright (c) 2019 DXBSS-MACLTP. All rights reserved.
//  Modify By:  * Ari Munandar
//              * arimunandar.dev@gmail.com
//              * https://github.com/arimunandar

import UIKit

protocol IFlightsRouter: class {
	// do someting...
}

class FlightsRouter: IFlightsRouter {	
	weak var view: FlightsViewController?
	
	init(view: FlightsViewController?) {
		self.view = view
	}
}
