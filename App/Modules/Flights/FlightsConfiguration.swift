//
//  FlightsConfiguration.swift
//  Sofa
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright (c) 2019 DXBSS-MACLTP. All rights reserved.
//  Modify By:  * Ari Munandar
//              * arimunandar.dev@gmail.com
//              * https://github.com/arimunandar

import Foundation
import UIKit

class FlightsConfiguration {
    static func initValues (controller : FlightsViewController , parameters: [String: Any] = [:]) {
        let router = FlightsRouter(view: controller)
        let presenter = FlightsPresenter(view: controller)
        let manager = FlightsManager()
        let interactor = FlightsInteractor(presenter: presenter, manager: manager)
        controller.interactor = interactor
        controller.router = router
        interactor.parameters = parameters
    }
}
