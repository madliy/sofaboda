//
//  HomeInteractor.swift
//  SofaBoda
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright (c) 2019 DXBSS-MACLTP. All rights reserved.
//  Modify By:  * Ari Munandar
//              * arimunandar.dev@gmail.com
//              * https://github.com/arimunandar

import UIKit

protocol IHomeInteractor: class {
	var parameters: [String: Any]? { get set }
    func getFromAriports(airportChar : String)
    func getToAriports(airportChar : String)
    func getAllJourneys(token : String , fromAirportCode : String , toAirportCode : String , date : String)
    
    
    
}

class HomeInteractor: IHomeInteractor {
    
    
    var presenter: IHomePresenter?
    var manager: IHomeManager?
    var parameters: [String: Any]?

    init(presenter: IHomePresenter, manager: IHomeManager) {
    	self.presenter = presenter
    	self.manager = manager
    }
    
    // MARK: - Methods to get data
    
    func getFromAriports(airportChar : String) {
        getAirport(airportChar: airportChar , fromAirport: true)
    }
    
    func getToAriports(airportChar : String) {
        getAirport(airportChar: airportChar , fromAirport: false)
    }
    
    func getAllJourneys(token: String, fromAirportCode: String, toAirportCode: String, date: String) {
        Services.getAllJourneys(token, fromAirportCode, toAirportCode, date) { (error, json) in
            self.presenter?.getJourneyList(json: json)
        }
    }
    
    private func getAirport(airportChar : String , fromAirport : Bool) {
        Services.getAirports(Constants.token, airportChar) { (error, json) in
            self.presenter?.getAirportsObject(json: json, fromAirport: fromAirport)
        }
    }
    
    
    
}
