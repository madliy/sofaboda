//
//  HomeViewController.swift
//  SofaBoda
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright (c) 2019 DXBSS-MACLTP. All rights reserved.
//  Modify By:  * Ari Munandar
//              * arimunandar.dev@gmail.com
//              * https://github.com/arimunandar

import UIKit
import SVProgressHUD

protocol IHomeViewController: class {
    var router: IHomeRouter? { get set }
    func setFromAirportList (airport : Airport!)
    func setToAirportList (airport : Airport!)
    
    func selectFromAirport (airport : Airport)
    func selectToAirport (airport : Airport)
    
    func setJourneyList(journeyList: [Journey])
    
}

class HomeViewController: BaseViewController , DropdownDelegate , UITextFieldDelegate , FlightTableDelegate {
    
    // MARK: - UI
    @IBOutlet weak var pickerView: UIView!
    @IBOutlet weak var expiryPicker: UIDatePicker!
    @IBOutlet weak var pickerBottomConstraints: NSLayoutConstraint!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var fromLoading: UIActivityIndicatorView!
    @IBOutlet weak var toLoading: UIActivityIndicatorView!
    @IBOutlet weak var toFlightTextField: UITextField!
    @IBOutlet weak var fromFlightTextField: UITextField!
    @IBOutlet weak var flightListContainer: UIView!
    
    
    // MARK: - variables
    var controller : DropDownTableViewController! = nil
    var fromAirport : Airport! = nil
    var toAirport : Airport! = nil
    var flightTableviewController : FlightsViewController! = nil
    
    var interactor: IHomeInteractor?
    var router: IHomeRouter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HomeConfiguration.initValues(controller: self)
        initValues()
    }
    
    // MARK: - initiate values
    
    func initValues() {
        expiryPicker.minimumDate = Date()
        controller = self.storyboard?.instantiateViewController(identifier: "dropdown")
        Colors.changePlaceHolderColor(textField: fromFlightTextField , color: Colors.transparentWhiteColor)
        Colors.changePlaceHolderColor(textField: toFlightTextField , color: Colors.transparentWhiteColor)
        controller.delegate = self
        
        dateLabel.text = expiryPicker.date.format(dateFormat: "dd MMM yyyy")
    }
    
    
    // MARK: - drop down delegae
    func selectFromAirport(airport : Airport) {
        fromFlightTextField.text = airport.name
        fromAirport = airport
        Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false) { (Timer) in
            DispatchQueue.main.async {
                self.view.endEditing(true)
            }
        }
    }
    
    func selectToAirport(airport : Airport) {
        toFlightTextField.text = airport.name
        toAirport = airport
        Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false) { (Timer) in
            DispatchQueue.main.async {
                self.view.endEditing(true)
            }
        }
    }
    
    
    // MARK: - text field delegae
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == fromFlightTextField) {
            fromFlightTextField.superview?.backgroundColor = Colors.textFocusColor
            toFlightTextField.superview?.backgroundColor = Colors.textUnfocusColor
            if(fromAirport != nil) {
                fromFlightTextField.text = fromAirport.code
            }
        }
        else if(textField == toFlightTextField) {
            toFlightTextField.superview?.backgroundColor = Colors.textFocusColor
            fromFlightTextField.superview?.backgroundColor = Colors.textUnfocusColor
            if(toAirport != nil) {
                toFlightTextField.text = toAirport.code
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        
        let newLength = text.count + string.count - range.length
        if(newLength < 3) {
            controller.hide()
        }
        else if (newLength == 3 && range.length == 0) {
            let airportCode = "\(text)\(string)"
            
            if(textField == fromFlightTextField) {
                fromLoading.startAnimating()
                fromAirport = nil
                interactor?.getFromAriports(airportChar: airportCode)
            }
            else if(textField == toFlightTextField) {
                toLoading.startAnimating()
                toAirport = nil
                interactor?.getToAriports(airportChar: airportCode)
            }
            
            
        }
        return true
    }
    
    
    
    
    
    // MARK: - show dropdown
    func showPopup(_ list: [Airport] , fromAirport : Bool , sender: UIView) {
        controller.setValues(list , fromAirport: fromAirport)
        controller.preferredContentSize = CGSize(width: 300, height: 200)
        showPopup(sourceView : sender)
    }
    
    private func showPopup(sourceView: UIView) {
        let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
        presentationController.sourceView = sourceView
        presentationController.sourceRect = sourceView.bounds
        presentationController.permittedArrowDirections = [.down, .up]
        self.present(controller, animated: true)
    }
    
    
    // MARK: - search action
    
    @IBAction func searchFlightsButtonAction(_ sender: Any) {
        if(fromAirport == nil || toAirport == nil) {
            showAlert(message: "Please select source and distnation airports")
        }
        else {
            self.flightListContainer.isHidden = true
            SVProgressHUD.show()
            interactor?.getAllJourneys(token: Constants.token, fromAirportCode: fromAirport.code, toAirportCode: toAirport.code, date: expiryPicker.date.format(dateFormat: "yyyy-MM-dd"))
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "flights") {
            flightTableviewController = (segue.destination as! FlightsViewController)
            flightTableviewController.delegate = self
        }
        if(segue.identifier == "map") {
            let viewController = segue.destination as! MapViewController
            viewController.fromAirport = fromAirport
            viewController.toAirport = toAirport
        }
    }
    
    
    // MARK: - show drop downs
    func setFromAirportList (airport : Airport!) {
        if(airport != nil) {
            self.showPopup([airport], fromAirport: true , sender : fromFlightTextField)
        }
        fromLoading.stopAnimating()
    }
    
    func setToAirportList(airport: Airport!) {
        if(airport != nil) {
            self.showPopup([airport], fromAirport: false , sender : toFlightTextField)
        }
        toLoading.stopAnimating()
    }
    
    func setJourneyList(journeyList: [Journey]) {
        SVProgressHUD.dismiss()
        if(journeyList.count == 0) {
            showAlert(message: "There is no flights please check again later")
        }
        else {
            flightTableviewController.interactor?.setJourneyList(journeyList: journeyList)
            self.flightListContainer.isHidden = false
        }
    }
    
    // MARK: - select flight
    
    func selectJourney() {
        self.navigate(type: .segue, module: GeneralRoute.map , segue: GeneralRoute.map.description, sender: nil)
    }
    
    
    // MARK: - select date
    
    @IBAction func opentDateButtonAction(_ sender: Any) {
        pickerBottomConstraints.constant =  0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        fromFlightTextField.resignFirstResponder()
        toFlightTextField.resignFirstResponder()
    }
    
    @IBAction func selectExpiryAction(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3) {
            self.pickerBottomConstraints.constant =  -266
            self.view.layoutIfNeeded()
        }
        dateLabel.text = expiryPicker.date.format(dateFormat: "dd MMM yyyy")
        
    }
    
}

extension HomeViewController: IHomeViewController {
    
}

extension HomeViewController {
    
}
