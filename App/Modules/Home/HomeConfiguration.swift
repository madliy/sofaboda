//
//  HomeConfiguration.swift
//  SofaBoda
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright (c) 2019 DXBSS-MACLTP. All rights reserved.
//  Modify By:  * Ari Munandar
//              * arimunandar.dev@gmail.com
//              * https://github.com/arimunandar

import Foundation
import UIKit

class HomeConfiguration {
    static func setup(parameters: [String: Any] = [:]) -> UIViewController {
        let controller = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "HomeViewController") as HomeViewController
        return controller
    }
    
    static func initValues (controller : HomeViewController , parameters: [String: Any] = [:]) {
        let router = HomeRouter(view: controller)
        let presenter = HomePresenter(view: controller)
        let manager = HomeManager()
        let interactor = HomeInteractor(presenter: presenter, manager: manager)
        controller.interactor = interactor
        controller.router = router
        interactor.parameters = parameters

    }
}
