//
//  HomePresenter.swift
//  SofaBoda
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright (c) 2019 DXBSS-MACLTP. All rights reserved.
//  Modify By:  * Ari Munandar
//              * arimunandar.dev@gmail.com
//              * https://github.com/arimunandar

import UIKit
import SwiftyJSON

protocol IHomePresenter: class {
    func getAirportsObject(json : JSON! , fromAirport : Bool)
    func getJourneyList(json : JSON!)
}

class HomePresenter: IHomePresenter {
    
    weak var view: IHomeViewController?
    
    init(view: IHomeViewController?) {
        self.view = view
    }
    
    func getAirportsObject(json : JSON! , fromAirport : Bool) {
        if(json == nil) {
            view?.setFromAirportList(airport: nil)
        }
        else {
            var airport : Airport! = nil
            let airportResourceJson = json["AirportResource"].dictionaryValue
            let airportsJson = airportResourceJson["Airports"]?.dictionaryValue
            if(airportsJson != nil) {
                airport = Airport(jsonObject: airportsJson!["Airport"]!)
            }
            if(fromAirport) {
                view?.setFromAirportList(airport: airport)
            }
            else {
                view?.setToAirportList(airport: airport)
            }
        }
    }
    
    func getJourneyList(json: JSON!) {
        if(json == nil) {
            view?.setJourneyList(journeyList: [])
        }
        else {
            var journeysArray = [Journey]()
            let scheduleResource = json["ScheduleResource"].dictionaryValue
            let schedule = scheduleResource["Schedule"]?.arrayValue
            if(schedule != nil) {
                for object in schedule! {
                    journeysArray.append(Journey(jsonObject: object))
                }
            }
            view?.setJourneyList(journeyList: journeysArray)
        }
    }
    
}
