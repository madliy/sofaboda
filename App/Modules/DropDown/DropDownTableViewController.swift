//
//  DropDownTableViewController.swift
//  Sofa
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright © 2019 DXBSS-MACLTP. All rights reserved.
//

import UIKit

protocol DropdownDelegate {
    func selectFromAirport(airport : Airport)
    func selectToAirport(airport : Airport)
}

class DropDownTableViewController: UITableViewController {
    
    var values = [Airport]()
    
    var delegate: DropdownDelegate?
    var fromAirport = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func setValues(_ values : [Airport] , fromAirport : Bool) {
        self.fromAirport = fromAirport
        self.values = values
        self.tableView.reloadData()
    }
    
    func hide() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return values.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = values[indexPath.row].name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(fromAirport) {
            delegate?.selectFromAirport(airport: values[indexPath.row])
        }
        else {
            delegate?.selectToAirport(airport: values[indexPath.row])
        }
        self.dismiss(animated: true)
    }
    
}
