//
//  SplashViewController.swift
//  Sofa
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright © 2019 DXBSS-MACLTP. All rights reserved.
//

import UIKit

class SplashViewController: BaseViewController {
    
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var movingPlaneHeight: NSLayoutConstraint!
    @IBOutlet weak var movingPlaneWidth: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.movingPlaneHeight.constant = self.view.frame.height + 50
        self.movingPlaneWidth.constant = self.view.frame.width + 50
        UIView.animate(withDuration: 3.0) {
            self.view.layoutIfNeeded()
        }
        self.widthConstraint.constant = 0
        UIView.animate(withDuration: 1.5) {
            self.view.layoutIfNeeded()
        }
        
        
        Timer.scheduledTimer(withTimeInterval: 4.0, repeats: false) { (Timer) in
            self.getToken()
        }
    }
    
    func getToken() {
        Services.getToken { (error, token) in
            if(token == nil) {
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "", message: "Please check internet connection", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { (UIAlertAction) in
                        self.getToken()
                    }))
                    alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                }
            }
            else {
                Constants.token = token!
                self.navigate(type: .segue, module: GeneralRoute.home , segue: GeneralRoute.home.description, sender: nil)
            }
        }
    }
    
    
    
    
    
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
}
