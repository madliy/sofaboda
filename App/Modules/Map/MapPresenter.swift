//
//  MapPresenter.swift
//  SofaBoda
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright (c) 2019 DXBSS-MACLTP. All rights reserved.
//  Modify By:  * Ari Munandar
//              * arimunandar.dev@gmail.com
//              * https://github.com/arimunandar

import UIKit

protocol IMapPresenter: class {
	// do someting...
}

class MapPresenter: IMapPresenter {	
	weak var view: IMapViewController?
	
	init(view: IMapViewController?) {
		self.view = view
	}
}
