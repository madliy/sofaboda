//
//  MapViewController.swift
//  SofaBoda
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright (c) 2019 DXBSS-MACLTP. All rights reserved.
//  Modify By:  * Ari Munandar
//              * arimunandar.dev@gmail.com
//              * https://github.com/arimunandar

import UIKit
import MapKit

protocol IMapViewController: class {
	var router: IMapRouter? { get set }
}

class MapViewController: BaseViewController , MKMapViewDelegate {
	var interactor: IMapInteractor?
	var router: IMapRouter?
    
    var fromAirport : Airport! = nil
    var toAirport : Airport! = nil
    @IBOutlet weak var map: MKMapView!

	override func viewDidLoad() {
        super.viewDidLoad()
        MapConfiguration.initValues(controller: self)
        
        map.delegate = self
        setPins(fromAirport: fromAirport, toAirport: toAirport)
        drawRoute(fromAirport: fromAirport, toAirport: toAirport)
        setCenterCoordinate(coordinate: CLLocationCoordinate2D(latitude: fromAirport.Latitude, longitude: fromAirport.Longitude), zoomLevel: 1, animated: true)
    }
    
    
    private func setCenterCoordinate(coordinate: CLLocationCoordinate2D, zoomLevel: Int, animated: Bool) {
        let span = MKCoordinateSpan(latitudeDelta: 0, longitudeDelta: 360 / pow(2, Double(zoomLevel)) * Double(self.view.frame.size.width) / 256)
        map.setRegion(MKCoordinateRegion(center: coordinate, span: span), animated: animated)
    }
    
    
    
    func setPins (fromAirport : Airport , toAirport : Airport){
        
        
        let fromCoordinate = CLLocationCoordinate2D(latitude: fromAirport.Latitude, longitude: fromAirport.Longitude)
        
        let toCoordinate = CLLocationCoordinate2D(latitude: toAirport.Latitude, longitude: toAirport.Longitude)
        
        let fromAnnotation = MKPointAnnotation()
        fromAnnotation.coordinate = fromCoordinate
        fromAnnotation.title = fromAirport.name
        map.addAnnotation(fromAnnotation)
        
        
        let toAnnotation = MKPointAnnotation()
        toAnnotation.coordinate = toCoordinate
        toAnnotation.title = toAirport.name
        map.addAnnotation(toAnnotation)
        
    }
    
    func drawRoute(fromAirport : Airport , toAirport : Airport) {
        let fromCoordinate = CLLocationCoordinate2D(latitude: fromAirport.Latitude, longitude: fromAirport.Longitude)
        let toCoordinate = CLLocationCoordinate2D(latitude: toAirport.Latitude, longitude: toAirport.Longitude)
        var points = [fromCoordinate, toCoordinate]
        let line = MKPolygon(coordinates: &points, count: points.count)
        map.addOverlay(line)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolygon {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = .orange
            polylineRenderer.lineWidth = 2
            return polylineRenderer
        }
        return MKOverlayRenderer(overlay: overlay)
    }
}

extension MapViewController: IMapViewController {
	// do someting...
}

extension MapViewController {
	// do someting...
}

extension MapViewController {
	// do someting...
}
