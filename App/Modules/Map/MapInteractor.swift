//
//  MapInteractor.swift
//  SofaBoda
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright (c) 2019 DXBSS-MACLTP. All rights reserved.
//  Modify By:  * Ari Munandar
//              * arimunandar.dev@gmail.com
//              * https://github.com/arimunandar

import UIKit

protocol IMapInteractor: class {
	var parameters: [String: Any]? { get set }
}

class MapInteractor: IMapInteractor {
    var presenter: IMapPresenter?
    var manager: IMapManager?
    var parameters: [String: Any]?

    init(presenter: IMapPresenter, manager: IMapManager) {
    	self.presenter = presenter
    	self.manager = manager
    }
}
