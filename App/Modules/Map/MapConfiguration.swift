//
//  MapConfiguration.swift
//  SofaBoda
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright (c) 2019 DXBSS-MACLTP. All rights reserved.
//  Modify By:  * Ari Munandar
//              * arimunandar.dev@gmail.com
//              * https://github.com/arimunandar

import Foundation
import UIKit

class MapConfiguration {
    static func setup(parameters: [String: Any] = [:]) -> UIViewController {
        let controller = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "MapViewController") as MapViewController
        return controller
    }
    
    
    static func initValues (controller : MapViewController , parameters: [String: Any] = [:]) {
        let router = MapRouter(view: controller)
        let presenter = MapPresenter(view: controller)
        let manager = MapManager()
        let interactor = MapInteractor(presenter: presenter, manager: manager)
        controller.interactor = interactor
        controller.router = router
        interactor.parameters = parameters
    }
    
}
