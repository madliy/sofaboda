//
//  GeneralRoute.swift
//  SofaBoda
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright (c) 2019 DXBSS-MACLTP. All rights reserved.
//  Modify By:  * Ari Munandar
//              * arimunandar.dev@gmail.com
//              * https://github.com/arimunandar

import Foundation
import UIKit

enum GeneralRoute: IRouter {
    /*
     If you want passing with parameters
     you just add like this:
     
     case sample
     case sample(parameter: [String: Any])
     
     you can use: String, Int, [String: Any], etc..
     */
    
    case map
    case home
    
    var description : String {
        switch self {
        // Use Internationalization, as appropriate.
        case .map: return "map"
        case .home: return "home"
        }
    }
    
}

extension GeneralRoute {
    var module: UIViewController? {
        /*
         Setup module with parameters like:
         
         switch self {
         case .sample:
         return SampleConfiguration.setup()
         case .sample(let parameters):
         return SampleConfiguration.setup(parameters: parameters)
         }
         
         */
        switch self {
        case .map:
            return MapConfiguration.setup()
        case .home:
            return HomeConfiguration.setup()
        }
    }
}
