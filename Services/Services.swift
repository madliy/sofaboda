//
//  Services.swift
//  Sofa
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright © 2019 DXBSS-MACLTP. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Services: NSObject {
    class func getToken(completion: @escaping (_ error: Error?, _ token:String?)->Void) {
        let url = Constants.domain + "oauth/token"
        
        let params   = ["client_id" : Constants.client_id,
                        "client_secret" : Constants.client_secret,
                        "grant_type" : Constants.grant_type] as [String : Any]
        
        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .validate(contentType: ["application/json"])
            .validate(statusCode: 200..<600)
            .responseJSON{ response in
                switch response.result
                {
                case .failure(let error):
                    completion(error, nil)
                case .success(let value):
                    let json = JSON(value)
                    let token = json["access_token"].stringValue
                    completion(nil , token)
                }
        }
    }
    
    class func getAirports(_ token : String , _ airportName : String , completion: @escaping (_ error: Error?, _ json : JSON?)->Void) {
        let url = Constants.domain + "mds-references/airports/\(airportName)?lang=en&limit=20&offset=0&LHoperated=0&Accept=application/json"
        
        let headers = ["Authorization" : "Bearer \(token)"]
        
        
        
        Alamofire.request(url, method: .get, parameters:nil, encoding: URLEncoding.default, headers: headers)
            .validate(contentType: ["application/json"])
            .validate(statusCode: 200..<600)
            .responseJSON{ response in
                switch response.result {
                case .failure(let error):
                    completion(error, nil)
                case .success(let value):
                    let json = JSON(value)
                    completion(nil , json)
                }
        }
    }
    
    
    
    class func getAllJourneys(_ token : String , _ fromAirportCode : String , _ toAirportCode : String , _ flightDate : String , completion: @escaping (_ error: Error?, _ json : JSON?)->Void) {
        let url = Constants.domain + "operations/schedules/\(fromAirportCode)/\(toAirportCode)/\(flightDate)?limit=20&directFlights=0&offset=0&Accept=application/json"
        
        let headers = ["Authorization" : "Bearer \(token)"]
        
        Alamofire.request(url, method: .get, parameters:nil, encoding: URLEncoding.default, headers: headers)
            .validate(contentType: ["application/json"])
            .validate(statusCode: 200..<600)
            .responseJSON{ response in
                switch response.result
                {
                case .failure(let error):
                    completion(error, nil)
                case .success(let value):
                    let json = JSON(value)
                    completion(nil , json)
                }
        }
    }
    
}
