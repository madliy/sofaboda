//
//  Journey.swift
//  Sofa
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright © 2019 DXBSS-MACLTP. All rights reserved.
//

import UIKit
import SwiftyJSON

class Journey: NSObject {
    
    var journeyCode = ""
    
    var flightsArray = [Flight]()
    
    init(jsonObject : JSON) {
        let totalJourney = jsonObject["TotalJourney"].dictionaryValue
        journeyCode = totalJourney["Duration"]!.stringValue
        let flightsObjectsArray = jsonObject["Flight"].arrayValue
        
        if(flightsObjectsArray.count != 0) {
            for object in flightsObjectsArray {
                flightsArray.append(Flight(jsonObject: object))
            }
        }
        else {
            let flightsObject = jsonObject["Flight"]
            print(flightsObject)
            flightsArray.append(Flight(jsonObject: flightsObject))
            
        }
    }
}
