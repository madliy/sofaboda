//
//  Airport.swift
//  Sofa
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright © 2019 DXBSS-MACLTP. All rights reserved.
//

import UIKit
import SwiftyJSON

class Airport: NSObject {
    var code = ""
    var name = ""
    var Latitude = 0.0
    var Longitude = 0.0
    
    init(jsonObject : JSON) {
        code = jsonObject["CityCode"].stringValue
        let Position = jsonObject["Position"].dictionaryValue
        let Coordinate = Position["Coordinate"]!.dictionaryValue
        Longitude = Coordinate["Longitude"]!.doubleValue
        Latitude = Coordinate["Latitude"]!.doubleValue
        let Names = jsonObject["Names"].dictionaryValue
        let Name = Names["Name"]!.dictionaryValue
        name = Name["$"]!.stringValue
    }
}
