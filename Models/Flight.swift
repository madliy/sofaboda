//
//  Flight.swift
//  Sofa
//
//  Created by DXBSS-MACLTP on 13/10/2019.
//  Copyright © 2019 DXBSS-MACLTP. All rights reserved.
//

import UIKit
import SwiftyJSON

class Flight: NSObject {
    
    var departureAirportCode = "--"
    var departureDate = "--"
    var departureTime = "--"
    var departureTerminalName = "--"
    
    var arrivalAirportCode = "--"
    var arrivalDate = "--"
    var arrivalTime = "--"
    var arrivalTerminalName = "--"
    
    init(jsonObject : JSON) {
        let Departure = jsonObject["Departure"].dictionaryValue
        var ScheduledTimeLocal = Departure["ScheduledTimeLocal"]?.dictionaryValue ?? nil
        var Terminal = Departure["Terminal"]?.dictionaryValue ?? nil
        
        departureAirportCode = Departure["AirportCode"]!.stringValue
        
        if(ScheduledTimeLocal != nil) {
            let DateTime = ScheduledTimeLocal!["DateTime"]?.stringValue ?? ""
            departureDate = DateTime.Date
            departureTime = DateTime.Time
        }
        
        if(Terminal != nil) {
            departureTerminalName = Terminal!["Name"]!.stringValue
        }
        
        // arrival
        
        let Arrival = jsonObject["Arrival"].dictionaryValue
        ScheduledTimeLocal = Arrival["ScheduledTimeLocal"]?.dictionaryValue ?? nil
        Terminal = Arrival["Terminal"]?.dictionaryValue ?? nil
        
        arrivalAirportCode = Arrival["AirportCode"]!.stringValue
        
        if(ScheduledTimeLocal != nil) {
            let DateTime = ScheduledTimeLocal!["DateTime"]?.stringValue ?? ""
            arrivalDate = DateTime.Date
            arrivalTime = DateTime.Time
        }
        
        if(Terminal != nil) {
            arrivalTerminalName = Terminal!["Name"]!.stringValue
        }
        
    }
    
}


